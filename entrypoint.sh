#!/usr/bin/env bash
if [[ -z "$1" ]]; then
	bash -i
else
	eval "bash -i -c -- '$*'"
fi

