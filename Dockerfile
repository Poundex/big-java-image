FROM ubuntu

ENV JDK_NAME="20-tem"
ENV GRAALVM_NAME="20.0.2-graalce"

ENV JAVA_HOME="/home/java/.sdkman/candidates/java/${JDK_NAME}"
ENV PATH="$JAVA_HOME/bin:${PATH}"
RUN echo "PATH=\"$PATH\"" > /etc/environment

RUN apt-get update
RUN apt-get install --assume-yes --force-yes zip unzip curl build-essential zlib1g-dev
RUN apt-get autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}
RUN useradd -m java

USER java
WORKDIR /home/java

RUN curl -o sdkman.sh  -s "https://get.sdkman.io" 
RUN chmod +x sdkman.sh
RUN ./sdkman.sh

RUN bash -i -c -- "sdk install java ${JDK_NAME}"

RUN bash -i -c -- "sdk install java ${GRAALVM_NAME}"
RUN bash -i -c -- "/home/java/.sdkman/candidates/java/${GRAALVM_NAME}/bin/gu install native-image"
RUN bash -i -c -- "sdk default java ${JDK_NAME}"

COPY entrypoint.sh /home/java/entrypoint.sh
